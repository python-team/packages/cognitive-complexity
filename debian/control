Source: cognitive-complexity
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Edward Betts <edward@4angle.com>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
 python3-setuptools,
Build-Depends-Indep:
 python3-pytest <!nocheck>,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://github.com/Melevir/cognitive_complexity
Vcs-Browser: https://salsa.debian.org/python-team/packages/cognitive-complexity
Vcs-Git: https://salsa.debian.org/python-team/packages/cognitive-complexity.git

Package: python3-cognitive-complexity
Architecture: all
Depends:
 ${misc:Depends},
 python3:any,
Description: Library to calculate Python functions cognitive complexity via code
 This library provides a mechanism to evaluate the cognitive complexity of
 Python functions. Cognitive complexity is a metric designed to measure the
 understandability of code, assessing how challenging it might be for a human
 to comprehend. It assigns points for various constructs such as breaking
 control flow, nesting, and recursion within the code. The calculation of
 cognitive complexity can aid in making the code more maintainable and readable
 by helping developers identify areas that could be simplified. The metric is
 derived from a structured algorithm that, although not precisely replicating
 the original proposal, delivers similar results. This algorithm adapts
 traditional computation techniques to assess elements that contribute to
 mental processing difficulty when reading and interpreting source code.
